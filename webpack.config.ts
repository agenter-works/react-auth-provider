import path from "path";
import webpack from "webpack";
import HtmlWebpackPlugin from "html-webpack-plugin";
import ForkTsCheckerWebpackPlugin from "fork-ts-checker-webpack-plugin";

import { config } from "dotenv";

config();

const webpackConfig: webpack.Configuration = {
  entry: path.join(__dirname, "demo/index.tsx"),
  devtool: "cheap-module-source-map",
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".jsx", ".json"],
  },
  mode: "development",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "index.js",
  },
  stats: {
    errorStack: true,
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        exclude: [/node_modules/],
        use: ["babel-loader"],
      },
      {
        test: /\.css$/,
        use: [{ loader: "style-loader" }, { loader: "css-loader" }],
      },
      {
        test: /\.(png|jpg|jpeg|gif|ico|svg)$/,
        loader: "file-loader",
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: "index.html",
      inject: false,
      template: path.join(__dirname, "demo/index.html"),
    }),

    new ForkTsCheckerWebpackPlugin({
      async: false,
      typescript: {
        configFile: "./tsconfig.json",
        configOverwrite: {
          compilerOptions: {
            module: "commonjs",
          },
        },
      },
    }),
    new webpack.EnvironmentPlugin({
      NODE_ENV: "development",
      DEBUG: false,
    }),
  ],
};

export default webpackConfig;
