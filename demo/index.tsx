import React from "react";
import ReactDOM from "react-dom";
import Login from "../src/login";
import Register from "../src/register";
import "bootstrap/dist/css/bootstrap.min.css";
import { Route, HashRouter, Switch } from "react-router-dom";
import PasswordRevcovery from "../src/password-recovery";
import PasswordReset from "../src/password-reset";
import config from "../src/config";
import { Field } from "formik";
import Logo from "../src/images/logo.svg";
import PasswordResetOtp from "../src/password-reset-otp";
import VerifyAccount from "../src/verify-account";

config.setUrl("https://localhost/id/");

config.setFirebaseConfig({
  apiKey: " AIzaSyAVPOF_oirKgjg0jtKppv2HKNYRTkAW7CM",
  authDomain: "agenterbooks.firebaseapp.com",
  serviceAccount:
    "firebase-adminsdk-ag5d2@agenterbooks.iam.gserviceaccount.com",
});

const SignIn = () => {
  return (
    <Login
      onLogin={(token) => {
        console.log(token);
      }}
      serviceName='books'
      appTitle='Agenter Bookkeeping'
      registerUrl='/signup'
      resetPasswordUrl='/forgot-password'
      termsUrl='https://www.agenterbooks.com/terms'
      protectionUrl='https://www.agenterbooks.com/data-protection'
      PrivacyUrl='https://www.agenterbooks.com/privacy'
      logo={Logo}
    />
  );
};

const PasswordRecoveryPage = () => {
  return (
    <PasswordRevcovery
      onRevcover={() => {}}
      onReset={() => {}}
      serviceName='books'
      signinUrl='/signin'
      appTitle='Agenter Bookkeeping'
      supportUrl='https://www.agenterbooks.com/contact'
      logo={Logo}
    />
  );
};

const RegisterPage = () => {
  return (
    <Register
      onRegister={(token) => {
        console.log(token);
      }}
      serviceName='books'
      appTitle='Agenter Bookkeeping'
      signinUrl='/'
      termsUrl='https://www.agenterbooks.com/terms'
      protectionUrl='https://www.agenterbooks.com/data-protection'
      PrivacyUrl='https://www.agenterbooks.com/privacy'
      resetPasswordUrl='/forgot-password'
      supportUrl='https://www.agenterbooks.com/contact'
      logo={Logo}
    />
  );
};

const PasswordResetPage = () => {
  return (
    <PasswordReset
      appTitle=''
      onReset={() => {}}
      serviceName='books'
      supportUrl='https://www.agenterbooks.com/contact'
      logo={Logo}
    />
  );
};

const VerifyAccountPage = () => {
  return (
    <VerifyAccount
      appTitle=''
      onVerify={() => {}}
      serviceName='books'
      signinUrl='/'
      supportUrl='https://www.agenterbooks.com/contact'
      logo={Logo}
    />
  );
};

const App = () => {
  return (
    <HashRouter>
      <Switch>
        <Route path='/' exact={true} component={SignIn} />
        <Route path='/signin' exact={true} component={SignIn} />
        <Route path='/signup' exact={true} component={RegisterPage} />
        <Route
          path='/forgot-password'
          exact={true}
          component={PasswordRecoveryPage}
        />

        <Route
          path='/reset-password/:token64/:email64'
          exact={true}
          component={PasswordResetPage}
        />

        <Route
          path='/verify-account/:token64/:email64'
          exact={true}
          component={VerifyAccountPage}
        />
      </Switch>
    </HashRouter>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
