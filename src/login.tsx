import {Field, Form, Formik, FormikHelpers, ErrorMessage, getIn} from "formik";
import React, {useState} from "react";
import {Link} from "react-router-dom";
import "./style.css";
import {Token} from "./types";
import FirbaseAuth from "./firebase-auth";
import {login} from "./api/auth";
import {Values} from "./api";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
  faEye,
  faEyeSlash,
  faLock,
  faSpinner,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";
import * as Yup from "yup";
import {error} from "console";
import Policy from "./policy";

const VALIDATION_SCHEMA = Yup.object()
  .shape({
    email: Yup.string()
      .nullable(false)
      .email("Enter a valid email.")
      .min(1, "Enter a valid email.")
      .max(50, "Enter a valid email.")
      .notOneOf([null, ""], "Email is required.")
      .required("Email is required."),
    password: Yup.string()
      .nullable(false)
      .min(7, "Password should contain minimum 7 characters.")
      .max(30, "Password cannot exceed 30 characters.")
      .notOneOf([null, ""], "Password is required.")
      .required("Password is required."),
  })
  .nullable(false)
  .required("Email and password fields are required.");
interface LoginFormValues extends Values {
  email: string;
  password: string;
}

interface LoginI {
  useSocial?: boolean;
  serviceName: string;
  appTitle: string;
  resetPasswordUrl?: string;
  registerUrl?: string;
  onLogin: (token: Token) => void;
  logo?: string;
  termsUrl?: string;
  protectionUrl?: string;
  PrivacyUrl?: string;
  networkAccess?: string;
}

interface Screen {
  name: string;
  error: string;
}

const Login = ({
  appTitle,
  resetPasswordUrl = "",
  registerUrl = "",
  termsUrl,
  protectionUrl,
  PrivacyUrl,
  logo,
  useSocial = true,
  networkAccess,
  ...props
}: LoginI) => {
  const [screen, setScreen] = React.useState<Screen>({
    name: "login",
    error: "",
  });
  const [showPassword, setShowPassword] = React.useState(false);

  const [errorClass, setErrorClass] = useState({});

  const handleSubmit = (
    values: LoginFormValues,
    actions: FormikHelpers<any>
  ) => {
    login(values)
      .then((response) => {
        props.onLogin(response.data);
      })
      .catch(({errors, message}) => {
        actions.setErrors(errors);
        actions.setSubmitting(false);
        if (errors.length === 0) {
          setScreen((prev) => {
            return {...prev, error: message};
          });
        }
      });
  };

  return (
    <div className="signin-body h-100">
      <section className="p-md-5 pt-2 pb-2 h-100 ">
        <div className="container h-100">
          <div className="row h-100 justify-content-center">
            <div className="col-md-5 px-4 m-md-auto flex-mob-limit pb-5 ">
              <div className="p-5 bg-white shadow-lg">
                <div className="d-flex flex-column align-items-center">
                  <div className="d-flex justify-content-center">
                    {logo && (
                      <img style={{width: "100px"}} src={logo} alt="Logo" />
                    )}
                    {/* <img
                          style={{ width: "35px" }}
                          src={BetaTag}
                          alt="Beta"
                        /> */}
                  </div>
                </div>
                <h5 className="f-14 text-center text-muted mt-2">
                  Log in to your account:
                </h5>
                <Formik
                  initialValues={{email: "", password: ""}}
                  onSubmit={handleSubmit}
                  validateOnChange={false}
                  validateOnBlur={false}
                  validationSchema={VALIDATION_SCHEMA}
                >
                  {({isSubmitting, errors, setErrors}) => {
                    return (
                      <Form>
                        <div
                          className={
                            errors.email
                              ? `form-group field-input mb-1 error-input-field`
                              : "form-group field-input mb-1"
                          }
                        >
                          <label className="col-form-label fw-600 labForBook">
                            Email
                          </label>
                          <div className="input-group">
                            <Field name="email" className="form-control mb-0" />
                          </div>
                          {/* <ErrorMessage
                            name='email'
                            component='span'
                            className='text-danger'
                          /> */}
                        </div>
                        <div
                          className={
                            errors.password
                              ? `form-group field-input mb-1 error-input-field`
                              : "form-group field-input mb-1"
                          }
                        >
                          <label className="col-form-label fw-600 labForBook">
                            Password
                          </label>
                          <div className="input-group">
                            <Field
                              name="password"
                              type={showPassword ? "text" : "password"}
                              className="form-control"
                            />
                            <div className="input-group-append">
                              <span className="input-group-text">
                                <FontAwesomeIcon
                                  onClick={() => setShowPassword(!showPassword)}
                                  icon={showPassword ? faEyeSlash : faEye}
                                />
                              </span>
                            </div>
                          </div>
                          {/* <ErrorMessage
                            name='password'
                            component='span'
                            className='text-danger'
                          /> */}
                        </div>

                        {(errors.email ||
                          errors.password ||
                          screen.error !== "") && (
                          <div className="container px-0">
                            <div className="d-flex justify-content-between alert alert-danger py-2">
                              <div className="d-flex align-item-center mx-0 py-0">
                                <ul className="list-unstyled error-message-body my-0 d-flex flex-column justify-content-center">
                                  <ErrorMessage
                                    name="email"
                                    component="li"
                                    className="text-danger"
                                  />
                                  <ErrorMessage
                                    name="password"
                                    component="li"
                                    className="text-danger"
                                  />

                                  <p className="text-danger mb-0">
                                    {screen.error}
                                  </p>
                                </ul>
                              </div>
                              <div className="">
                                <div
                                  className="d-flex justify-content-end align-items-center flex-grow-1 flex-fill"
                                  style={{height: "100%"}}
                                >
                                  <button
                                    onClick={() => setErrors({})}
                                    className="border-0 bg-transparent text-danger"
                                  >
                                    <FontAwesomeIcon icon={faTimes} />
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        )}
                        <div className="mt-4">
                          <div className="form-button-wrapper">
                            <button
                              disabled={isSubmitting}
                              type="submit"
                              value="Login"
                              className="my-sm-0 w-100  primary-large-button padding5 borderR4 font-weight-bold"
                            >
                              {isSubmitting ? (
                                <FontAwesomeIcon icon={faSpinner} spin />
                              ) : (
                                <>
                                  <FontAwesomeIcon
                                    className="marginRight5"
                                    icon={faLock}
                                  />
                                  <span>Login</span>
                                </>
                              )}
                            </button>
                          </div>
                        </div>
                      </Form>
                    );
                  }}
                </Formik>

                {useSocial === true && (
                  <>
                    <div className="d-flex justify-content-between mt-4 ">
                      <hr className="line d-inline " />
                      <span>OR</span>
                      <hr className="line d-inline " />
                    </div>
                    {networkAccess === "online" ? (
                      <div className="firebase_button_wrapper">
                        <FirbaseAuth
                          label="Sign in with google"
                          onSuccess={props.onLogin}
                        />
                      </div>
                    ) : null}
                  </>
                )}

                <div className="d-flex justify-content-between">
                  <div className="form-group text-center mt-3 mb-0">
                    <Link
                      to={resetPasswordUrl}
                      title="Forgot password"
                      className="text-decoration-none linksopn text-link-color"
                    >
                      Forgot password
                    </Link>
                  </div>
                  <div className="form-group text-center mt-3 mb-0">
                    <Link
                      to={registerUrl}
                      title="Register"
                      className="text-decoration-none text-link-color linksopn"
                    >
                      {/* New to {appTitle}? */}
                      Register
                    </Link>
                  </div>
                </div>
                <Policy
                  appTitle={appTitle}
                  termsUrl={termsUrl}
                  protectionUrl={protectionUrl}
                  PrivacyUrl={PrivacyUrl}
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Login;
