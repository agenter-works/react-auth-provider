import { Field, Form, Formik } from 'formik';
import React from 'react';
import { useLocation, useParams } from 'react-router-dom';
// import { URLSearchParams } from 'url';
import PasswordResetOtp from './password-reset-otp';
import './style.css';
import { base64DecodeUrl } from './util';

interface PasswordResetFormValues {
  password: string;
  code: string;
}

interface PasswordResetI {
  serviceName: string;
  appTitle: string;
  supportUrl: string;
  onReset: () => void;
  logo?: string;
}

const PasswordReset = ({
  appTitle,
  serviceName,
  supportUrl,
  logo,
  onReset,
  ...props
}: PasswordResetI) => {
  const { token64 }: { token64: string } = useParams();
  const { email64 }: { email64: string } = useParams();

  const email: string = base64DecodeUrl(email64);
  const code: string = token64.slice(-6);
  const token: string = token64.slice(0, token64.length - 6);

  if (token64.length < 30 || email.length < 5) {
    return (
      <>
        <h3>Broken URL</h3>
      </>
    );
  }
  const { search } = useLocation();

  const query = React.useMemo(() => new URLSearchParams(search), [search]);

  const user_type = query.get('user_type');

  let signinUrl = '/';

  if (user_type) {
    signinUrl += `&user_type=${user_type}`;
  }

  return (
    <div className='signin-body h-100'>
      <section className='p-md-5 pt-2 pb-2 h-100 '>
        <div className='container h-100'>
          <div className='row h-100 justify-content-center'>
            <div className='col-md-5 px-4 m-md-auto flex-mob-limit pb-5 '>
              <div className='p-5 bg-white shadow-lg'>
                <div className='d-flex flex-column align-items-center mb-3'>
                  <h6>Can't Login?</h6>
                  <div className='d-flex justify-content-center'>
                    <img style={{ width: '100px' }} src={logo} alt='Logo' />
                  </div>
                </div>
                <PasswordResetOtp
                  appTitle={appTitle}
                  serviceName={serviceName}
                  signinUrl={signinUrl}
                  token={token}
                  email={email}
                  code={code}
                  logo={logo}
                  onReset={onReset}
                />
                <div className='text-center'>
                  <a
                    href={supportUrl}
                    className='text-decoration-none text-link-color fw-600'
                    target='_blank'
                  >
                    Contact us
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default PasswordReset;
