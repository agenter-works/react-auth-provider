import { Field, Form, Formik, FormikHelpers, ErrorMessage } from 'formik';
import React from 'react';
import { Link } from 'react-router-dom';
import './style.css';
import { Token } from './types';
import FirbaseAuth from './firebase-auth';
import { Values } from './api';
import { register } from './api/auth';
import COUNTRY_LIST from './country';
import * as Yup from 'yup';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faEye,
  faEyeSlash,
  faLock,
  faSpinner,
  faTimes,
} from '@fortawesome/free-solid-svg-icons';
import Policy from './policy';

const phoneRegExp =
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
const VALIDATION_SCHEMA = Yup.object().shape({
  password: Yup.string()
    .min(7, 'Password should contain minimum 7 characters.')
    .max(30, 'Password cannot exceed 30 characters.')
    .required('Password is required.'),
  mobile_number: Yup.string()
    .min(6, 'Enter a valid mobile number.')
    .max(15, 'Enter a valid mobile number.')
    .matches(phoneRegExp, 'Enter a valid mobile number.')
    .required('Mobile number is required.'),
  display_name: Yup.string()
    .max(50, 'Name cannot exceed 50 characters.')
    .required('Name is required.'),
  email: Yup.string()
    .email('Enter a valid email.')
    .max(50, 'Enter a valid email address.')
    .required('Email is required.'),
});

interface RegisterFormValues extends Values {
  email: string;
  password: string;
  display_name: string;
  country: string;
  mobile_number: string;
}

interface RegisterI {
  useSocial?: boolean;
  serviceName: string;
  appTitle: string;
  resetPasswordUrl?: string;
  signinUrl?: string;
  termsUrl?: string;
  protectionUrl?: string;
  PrivacyUrl?: string;
  supportUrl: string;
  onRegister: (token: Token) => void;
  logo?: string;
}

interface Screen {
  name: string;
  error: string;
}

const Register = ({
  appTitle,
  resetPasswordUrl = '',
  signinUrl = '',
  termsUrl,
  protectionUrl,
  PrivacyUrl,
  supportUrl,
  useSocial = true,
  logo,
  ...props
}: RegisterI) => {
  const [screen, setScreen] = React.useState<Screen>({
    name: 'register',
    error: '',
  });
  const [showPassword, setShowPassword] = React.useState(false);

  const handleSubmit = (
    values: RegisterFormValues,
    actions: FormikHelpers<any>
  ) => {
    register(values)
      .then((response) => {
        props.onRegister(response.data);
      })
      .catch(({ errors, message }) => {
        actions.setErrors(errors);
        actions.setSubmitting(false);
        if (errors.length === 0) {
          setScreen((prev) => {
            return { ...prev, error: message };
          });
        }
      });
  };

  function validatePassword(value: any) {
    let minLength: number = 8;
    let error;
    if (value && value.length > 0) {
      if (!/[A-Z]/.test(value)) {
        error = `Password must be at least 1 upper case`;
      } else if (!/[a-z]/.test(value)) {
        error = `Password must be at least 1 lower case`;
      } else if (!/[0-9]/.test(value)) {
        error = `Password must be at least one digit`;
      } else if (!/[#?!@$%^&*-]/.test(value)) {
        error = `Password must be at least one special character`;
      } else if (value.length < minLength) {
        error = `Password must be at least ${minLength} characters long`;
      } else {
        error = '';
      }
      return error;
    }
  }

  return (
    <div className='signin-body h-100'>
      <section className='p-md-5 pt-2 pb-2 h-100'>
        <div className='container h-100'>
          <div className='row h-100 justify-content-center'>
            <div className='col-md-5 px-4 m-md-auto flex-mob-limit pb-5'>
              <div className='p-5 bg-white shadow-lg'>
                <div className='d-flex flex-column align-items-center'>
                  <div className='d-flex justify-content-center'>
                    {logo && (
                      <img style={{ width: '100px' }} src={logo} alt='Logo' />
                    )}
                    {/* <img
                          style={{ width: "35px" }}
                          src={BetaTag}
                          alt="Beta"
                        /> */}
                  </div>
                </div>
                <h5 className='f-14 text-center text-muted mt-2'>
                  Sign up for your account:
                </h5>
                <Formik
                  initialValues={{
                    email: '',
                    password: '',
                    display_name: '',
                    country: 'IN',
                    mobile_number: '',
                  }}
                  onSubmit={handleSubmit}
                  validateOnChange={false}
                  validateOnBlur={false}
                  validationSchema={VALIDATION_SCHEMA}
                >
                  {({ isSubmitting, errors, setErrors }) => {
                    return (
                      <Form>
                        <div
                          className={
                            errors.display_name
                              ? `form-group field-input mb-1 error-input-field`
                              : 'form-group field-input mb-1'
                          }
                        >
                          <label className='col-form-label fw-600 labForBook'>
                            Name
                          </label>
                          <div className='input-group'>
                            <Field
                              name='display_name'
                              className='form-control mb-0'
                            />
                          </div>
                        </div>

                        <div
                          className={
                            errors.email
                              ? `form-group field-input mb-1 error-input-field`
                              : 'form-group field-input mb-1'
                          }
                        >
                          <label className='col-form-label fw-600 labForBook'>
                            Email
                          </label>
                          <div className='input-group'>
                            <Field
                              name='email'
                              className='form-control mb-0'
                              autoComplete='disabled'
                            />
                          </div>
                        </div>

                        <div
                          className={
                            errors.country
                              ? `form-group field-input mb-1 error-input-field`
                              : 'form-group field-input mb-1'
                          }
                        >
                          <label className='col-form-label fw-600 labForBook'>
                            Country
                          </label>
                          <div className='input-group'>
                            <Field
                              name='country'
                              as='select'
                              className='form-control mb-0'
                            >
                              {COUNTRY_LIST.map((country) => (
                                <option
                                  key={country.value}
                                  value={country.value}
                                >
                                  {country.name}
                                </option>
                              ))}
                            </Field>
                          </div>
                        </div>

                        <div
                          className={
                            errors.mobile_number
                              ? `form-group field-input mb-1 error-input-field`
                              : 'form-group field-input mb-1'
                          }
                        >
                          <label className='col-form-label fw-600 labForBook'>
                            Mobile Number
                          </label>
                          <div className='input-group'>
                            <Field
                              name='mobile_number'
                              className='form-control mb-0'
                            />
                          </div>
                        </div>

                        <div
                          className={
                            errors.password
                              ? `form-group field-input mb-1 error-input-field`
                              : 'form-group field-input mb-1'
                          }
                        >
                          <label className='col-form-label fw-600 labForBook'>
                            Password
                          </label>
                          <div className='input-group'>
                            <Field
                              name='password'
                              type={showPassword ? 'text' : 'password'}
                              className='form-control mb-0'
                              validate={validatePassword}
                              autoComplete='disabled'
                            />
                            <div className='input-group-append'>
                              <span
                                onClick={() => setShowPassword(!showPassword)}
                                className='input-group-text pointer'
                              >
                                <FontAwesomeIcon
                                  icon={showPassword ? faEyeSlash : faEye}
                                />
                              </span>
                            </div>
                          </div>
                        </div>

                        {(errors.display_name ||
                          errors.email ||
                          errors.country ||
                          errors.mobile_number ||
                          errors.password ||
                          screen.error !== '') && (
                          <div className='container px-0'>
                            <div className='d-flex justify-content-between alert alert-danger py-2'>
                              <div className='d-flex align-item-center mx-0 py-0'>
                                <ul className='list-unstyled error-message-body my-0 d-flex flex-column justify-content-center'>
                                  <ErrorMessage
                                    name='display_name'
                                    component='span'
                                    className='text-danger'
                                  />
                                  <ErrorMessage
                                    name='email'
                                    component='span'
                                    className='text-danger'
                                  />
                                  <ErrorMessage
                                    name='country'
                                    component='span'
                                    className='text-danger'
                                  />
                                  <ErrorMessage
                                    name='mobile_number'
                                    component='span'
                                    className='text-danger'
                                  />
                                  <ErrorMessage
                                    name='password'
                                    component='span'
                                    className='text-danger'
                                  />
                                  <p className='text-danger mb-0'>
                                    {screen.error}
                                  </p>
                                </ul>
                              </div>
                              <div className=''>
                                <div
                                  className='d-flex justify-content-end align-items-center flex-grow-1 flex-fill'
                                  style={{ height: '100%' }}
                                >
                                  <button
                                    onClick={() => setErrors({})}
                                    className='border-0 bg-transparent text-danger'
                                  >
                                    <FontAwesomeIcon icon={faTimes} />
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        )}

                        <div className='mt-4'>
                          <div className='form-button-wrapper'>
                            <button
                              disabled={isSubmitting}
                              type='submit'
                              value='Register'
                              className='my-sm-0 w-100  primary-large-button padding5 borderR4 font-weight-bold'
                            >
                              {isSubmitting ? (
                                <FontAwesomeIcon icon={faSpinner} spin />
                              ) : (
                                <>
                                  <FontAwesomeIcon
                                    className='marginRight5'
                                    icon={faLock}
                                  />
                                  <span>Register</span>
                                </>
                              )}
                            </button>
                          </div>
                        </div>
                      </Form>
                    );
                  }}
                </Formik>
                {useSocial === true &&
                <>
                <div className='d-flex justify-content-between align-items-center mt-4 '>
                  <hr className='line d-inline ' />
                  <span className='f-12 text-muted'>OR</span>
                  <hr className='line d-inline ' />
                </div>
                <div className='firebase_button_wrapper'>
                  <FirbaseAuth
                    label='Sign up with google'
                    onSuccess={props.onRegister}
                  />
                </div>
                </>
                }
                <div className='mt-4'>
                  <div className='form-group text-center mt-4 mb-0'>
                    Have an account?
                    <Link
                      to={signinUrl}
                      className='text-decoration-none linksopn'
                    >
                      <span className='text-link-color'> Sign in</span>
                    </Link>
                  </div>
                </div>
                <Policy
                  appTitle={appTitle}
                  termsUrl={termsUrl}
                  protectionUrl={protectionUrl}
                  PrivacyUrl={PrivacyUrl}
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Register;
