import { Field, Form, Formik, FormikHelpers, ErrorMessage } from "formik";
import React from "react";
import { Link } from "react-router-dom";
import { resetPassword } from "./api/auth";
import { Values } from "./api";
import "./style.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import Otp from './otp';

interface PasswordResetFormValues extends Values {
  password: string;
  code: string;
}

interface PasswordResetI {
  serviceName: string;
  appTitle: string;
  token: string;
  email: string;
  signinUrl: string;
  code?: string;
  logo?: string;
  onReset: () => void;
}

interface Screen {
  name: string;
  error: string;
}

const PasswordResetOtp = ({
  appTitle,
  signinUrl,
  logo,
  onReset,
  ...props
}: PasswordResetI) => {
  const [screen, setScreen] = React.useState<Screen>({
    name: "password-otp",
    error: "",
  });
  
  const handleSubmit = (
    values: PasswordResetFormValues,
    actions: FormikHelpers<any>
  ) => {
    // setIsSuccess(true);
    resetPassword({
      ...values,
      type: "email",
      token: props.token,
      email: props.email,
    })
      .then((response) => {
        setScreen((prev) => {
          return { ...prev, name: "success" };
        });

        onReset();
      })
      .catch(({ errors, message }) => {
        actions.setErrors(errors);
        actions.setSubmitting(false);
        if (errors.length === 0) {
          setScreen((prev) => {
            return { ...prev, error: message };
          });
        }
      });
  };

  if (screen.name === "success") {
    return (
      <>
        <div className='text-center'>
          <h3 className='signup-head'>Password reset success</h3>
          <div className='row justify-content-center py-3'>
            <div className='text-center'>
              <p className='mb-2 text-muted'>
                Your password successfully reset
              </p>
              <p className='fw-600'>
                <Link
                  to={signinUrl}
                  title='Sign In'
                  className='text-decoration-none text-link-color'>
                  Sign In
                </Link>
              </p>
            </div>
          </div>
        </div>
      </>
    );
  }

  return (
    <>
      <Formik
        initialValues={{ code: props.code ?? "", password: "" }}
        onSubmit={handleSubmit}>
        {({ isSubmitting, setFieldValue }) => {
          return (
            <Form>
              {!props.code === true && (
                <div className='form-group field-input-register'>
                  <label className='col-form-label fw-600 labForBook'>
                    Enter 6 digit OTP recived on your email
                  </label>
                  <Otp onChange={(val) => setFieldValue('code', val)} />
                  <ErrorMessage
                    name='code'
                    component='span'
                    className='text-danger'
                  />
                </div>
              )}

              <div className='form-group field-input-register'>
                <label className='col-form-label fw-600 labForBook'>
                  New password
                </label>
                <Field name='password' className='form-control' />
                <ErrorMessage
                  name='password'
                  component='span'
                  className='text-danger'
                />
              </div>
              {screen.error !== "" && (
                <span className='text-danger'> {screen.error}</span>
              )}
              <div className='mt-4'>
                <button
                  disabled={isSubmitting}
                  type='submit'
                  value='Reset'
                  className='my-sm-0 w-100  primary-large-button padding5 borderR4 font-weight-bold'>
                  {isSubmitting ? (
                    <FontAwesomeIcon icon={faSpinner} spin />
                  ) : (
                    "Reset"
                  )}
                </button>
              </div>
            </Form>
          );
        }}
      </Formik>
    </>
  );
};

export default PasswordResetOtp;
