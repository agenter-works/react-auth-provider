
/**
 * Use this to recreate a Base64 encoded string that was made URL friendly
 * using Base64EncodeurlFriendly.
 * '-' and '_' are replaced with '+' and '/' and also it is padded with '+'
 *
 * @param {String} str the encoded string
 * @returns {String} the URL friendly encoded String
 */
 export const base64DecodeUrl = (str: string): string => {

	let remainder;
	if (remainder = str.length % 4) {
		str += '='.repeat(4 - remainder);
	}
	return atob(str);
  };