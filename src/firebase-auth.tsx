import React from 'react';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import { registerWithProvider } from './api/auth';
import config from './config';

// Configure FirebaseUI.

interface FirbaseAuthI {
  onSuccess: (token: any) => void;
  label: string;
}

let _INIT_: boolean = false;

const FirbaseAuth = ({ onSuccess, label }: FirbaseAuthI) => {
  const [screen, setScreen] = React.useState<string>('init');

  const uiConfig = {
    // Popup signin flow rather than redirect flow.
    signInFlow: 'redirect',
    // We will display Google and Facebook as auth providers.
    signInOptions: [
      {
        provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        providerName: 'Google',
        // To override the full label of the button.
        fullLabel: label,
      },
    ],
    fullLabel: 'Google signin',
    callbacks: {
      // Avoid redirects after sign-in.
      signInSuccessWithAuthResult: () => false,
    },
  };

  if (_INIT_ === false) {
    // Configure Firebase.
    firebase.initializeApp(config.getFirebaseConfig());
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.NONE);
    _INIT_ = true;
  }

  const serviceLogin = (user: firebase.User) => {
    setScreen('verify-request');
    user.getIdToken().then((token) => {
      registerWithProvider('firebase', token).then((response) => {
        setScreen('complete');
        onSuccess(response.data);
      });
    });
  };

  // Listen to the Firebase Auth state and set the local state.
  React.useEffect(() => {
    const unregisterAuthObserver = firebase
      .auth()
      .onAuthStateChanged((user) => {
        if (user) {
          serviceLogin(user);
        }
      });
    return () => unregisterAuthObserver(); // Make sure we un-register Firebase observers when the component unmounts.
  }, []);

  if (screen === 'verify-request') {
    return <></>;
  }

  return (
    <StyledFirebaseAuth
      uiCallback={(ui) => ui.disableAutoSignIn()}
      uiConfig={uiConfig}
      firebaseAuth={firebase.auth()}
    />
  );
};

export default FirbaseAuth;
