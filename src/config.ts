
interface ConfigI {
    // url: string;
    setUrl: (url: string) => void;
    getUrl: () => string;
    setFirebaseConfig: (config: any) => void;
    getFirebaseConfig: () => string;
}

let API_URL: string = '';
let FIREBASE_CONFIG: any = {};

const Config: ConfigI = {
    // url: s = '',
    setUrl: (url: string) => {
        API_URL = url;
    },
    getUrl: (): string => {
        return API_URL;
    },
    setFirebaseConfig: (config: any) => {
        FIREBASE_CONFIG = config;
    },
    getFirebaseConfig: (): any => {
        return FIREBASE_CONFIG;
    }
}

export default Config;