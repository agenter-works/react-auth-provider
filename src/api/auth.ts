import { endPoints } from './endpoints';
import { getData, postData, Values } from '../api';

const register = async (data: Values) => {
  return postData({
    url: endPoints.register,
    data: { ...data, 'app-id': 'books' },
    params: { type: 'email' },
    headers: {
      'Content-Type': 'application/json',
    },
  });
};

const registerOnInvitation = async (data: any) => {
  return postData({
    url: endPoints.registerOnInvitation,
    data: { ...data, 'app-id': 'books' },
    headers: {
      'Content-Type': 'application/json',
    },
  });
};

const registerWithProvider = async (provider: string, token: any) => {
  return postData({
    url: endPoints.registerWithProvider,
    data: { provider_name: provider, token: token, 'app-id': 'books' },
    headers: {
      'Content-Type': 'application/json',
    },
  });
};

const login = (data: Values) => {
  return postData({
    url: endPoints.login,
    data: { ...data, 'app-id': 'books' },
    params: { type: 'email' },
    headers: {
      'Content-Type': 'application/json',
    },
  });
};

const profile = async (id?: number) => {
  return await getData({
    url: endPoints.profile,
    id,
  });
};

const recoverPasswordByEmail = async (email: string) => {
  return postData({
    url: endPoints.recoverPassword,
    data: { email },
    headers: {
      'Content-Type': 'application/json',
    },
  });
};

const resetPassword = async (values: Values) => {
  return postData({
    url: endPoints.resetPassword,
    data: values,
    headers: {
      'Content-Type': 'application/json',
    },
  });
};

const accountVerify = async (values: Values) => {
  return postData({
    url: endPoints.accountVerify,
    data: values,
    headers: {
      'Content-Type': 'application/json',
    },
  });
};

export {
  register,
  login,
  profile,
  recoverPasswordByEmail,
  resetPassword,
  registerOnInvitation,
  registerWithProvider,
  accountVerify,
};
