import axios from "axios";
import config from '../config';

export interface Values {
  [key: string]: string | number | boolean;
}

const getUrl = (segment: string = '') => {
  return config.getUrl() + segment;
}

const instance = axios.create({
  timeout: 20000,
  withCredentials: true,
});

instance.interceptors.response.use(response => {
  // Any status code that lie within the range of 2xx cause this function to trigger
  // Do something with response data
  return response;
}, error => {
  // Any status codes that falls outside the range of 2xx cause this function to trigger
  // Do something with response error

  const errors = error.response?.data?.errors ?? [];
  const message = error.response?.data?.message ?? '';
  throw { errors, message };
});

export default instance;

export interface ReadI {
  url: string;
  id?: number | string;
  params?: any;
}

export interface CreateI extends ReadI {
  data?: any;
  headers?: any;
}

const getData = async ({ url, params, id }: ReadI) => {

  if (id) {
    url = `${url}/${id}`;
  }

  return await instance.get(getUrl(url), {
    params,
  });
};

const postData = async ({ url, data, params, id }: CreateI) => {

  if (id) {
    url = `${url}/${id}`;
  }

  return await instance.post(getUrl(url), data, {
    params,
  });
};

export { postData, getData };
