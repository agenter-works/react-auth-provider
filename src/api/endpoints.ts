const endPoints = {
  register: 'register',
  registerWithProvider: 'register/provider',
  registerOnInvitation: 'register/set-password',
  login: 'login',
  profile: 'profile',
  recoverPassword: 'recover-password',
  resetPassword: 'reset-password',
  accountVerify: 'verification/verify',
};

export { endPoints };
