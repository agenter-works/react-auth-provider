import { Field, Form, Formik } from "formik";
import React from "react";
import { useParams, Link } from "react-router-dom";
import VerifyAccountOtp from "./verify-account-otp";
import "./style.css";
import { base64DecodeUrl } from "./util";

interface VerifyAccountI {
  serviceName: string;
  appTitle: string;
  supportUrl: string;
  signinUrl: string;
  onVerify: () => void;
  logo?: string;
}

const VerifyAccount = ({
  appTitle,
  serviceName,
  supportUrl,
  signinUrl,
  logo,
  onVerify,
  ...props
}: VerifyAccountI) => {
  const { token64 }: { token64: string } = useParams();
  const { email64 }: { email64: string } = useParams();
  console.log(useParams());
  const email: string = base64DecodeUrl(email64);
  const code: string = token64.slice(-6);
  const token: string = token64.slice(0, token64.length - 6);

  if (token64.length < 30 || email.length < 5) {
    return (
      <>
        <h3>Broken URL</h3>
      </>
    );
  }

  return (
    <div className='signin-body h-100'>
      <section className='p-md-5 pt-2 pb-2 h-100 '>
        <div className='container h-100'>
          <div className='row h-100 justify-content-center'>
            <div className='col-md-5 px-4 m-md-auto flex-mob-limit pb-5 '>
              <div className='p-5 bg-white shadow-lg'>
                <div className='d-flex flex-column align-items-center mb-3'>
                  <h6>Verify your account</h6>
                  <div className='d-flex justify-content-center'>
                    <img style={{ width: "100px" }} src={logo} alt='Logo' />
                  </div>
                </div>
                <VerifyAccountOtp
                  appTitle={appTitle}
                  serviceName={serviceName}
                  token={token}
                  email={email}
                  code={code}
                  logo={logo}
                  onVerify={onVerify}
                />
                <div className='text-center'>
                  <Link
                    to={signinUrl}
                    className='text-decoration-none text-link-color fw-600'
                    >
                    Return to login
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default VerifyAccount;
