import React from "react";

interface OtpI {
    inputSize: number;
    isInputNum: boolean;
    onChange: (value: string) => void;
}

const Otp = ({
    inputSize,
    isInputNum,
    onChange
}: OtpI) => {

    const [code, setCode] = React.useState([...Array(inputSize)].map(() => ""));
    const inputs = React.useRef<Array<any>>([]);


  const isInputValueValid = (value: string) => {
    const isTypeValid = isInputNum ? !isNaN(parseInt(value, 10)) : typeof value === 'string';

    return isTypeValid && value.trim().length === 1;
  }

    const processInput = (e: any, slot: number) => {

        const {value} = e.target;

        if (!isInputValueValid(value)) {
            return;
        }

        const newCode = [...code];
        newCode[slot] = value;
        setCode(newCode);
        if (inputs.current && slot != inputSize - 1 && e.target.value.length === 1) {
          inputs.current[slot + 1].focus();
        }
      };
      React.useEffect(() => {
        onChange(code.join(""));
      }, [code]);
    
      const onKeyUp = (e: React.KeyboardEvent, slot: number) => {
        if (e.key == "Backspace") {
          const newCode = [...code];
            console.log(newCode)
            newCode[slot] = "";
            setCode(newCode);
            if (slot > 0) {
                inputs.current[slot - 1].focus();
            }
        } else if (e.key === 'ArrowLeft') {
            if (slot > 0) {
                inputs.current[slot - 1].focus();
            }
            
        } else if (e.key === 'ArrowRight') {
            if (slot < (inputSize - 1)) {
                inputs.current[slot + 1].focus();
            }
        } 
      };
   

    return (
        <div className="input-group">
            {
                code.map((num, idx) => <input
                    key={idx}
                    type='text'
                    inputMode={isInputNum ? 'numeric' : 'text'}
                    className='form-control'
                    maxLength={1}
                    value={num}
                    autoFocus={!code[0].length && idx == 0}
                    onChange={(e) => processInput(e, idx)}
                    onKeyUp={(e) => onKeyUp(e, idx)}
                    ref={(ref: any) => inputs.current.push(ref)}
                    onFocus={(e) => {
                        e.target.select();
                    }}
                  />
                )
            }
        </div>
    );
} 

Otp.defaultProps = {
    isInputNum: true,
    inputSize: 6
}

export default Otp;

