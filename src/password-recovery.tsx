import { Field, Form, Formik, FormikHelpers, ErrorMessage } from "formik";
import React from "react";
import { Link } from "react-router-dom";
import PasswordResetOtp from "./password-reset-otp";
import { recoverPasswordByEmail } from "./api/auth";
import "./style.css";
import * as Yup from "yup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner, faTimes } from "@fortawesome/free-solid-svg-icons";

// import forgetImage from "./images/reset-image.svg";

const VALIDATION_SCHEMA = Yup.object().shape({
  email: Yup.string()
    .email()
    .max(50, "Email is too long!")
    .required("Email is required!"),
});

interface PasswordRevcoveryFormValues {
  email: string;
}

interface PasswordRevcoveryI {
  serviceName: string;
  appTitle: string;
  signinUrl: string;
  supportUrl?: string;
  onRevcover: () => void;
  onReset: () => void;
  logo?: string;
}

interface Screen {
  isSend: boolean;
  token: string;
  ttl: number;
  email: string;
  resendTtl: number;
}

const SendRecovery = ({
  appTitle,
  supportUrl = "",
  signinUrl,
  onSend,
  logo,
}: {
  appTitle: string;
  logo?: string;
  signinUrl: string;
  supportUrl?: string;
  onSend: (
    email: string,
    token: string,
    ttl: number,
    resendTtl: number
  ) => void;
}) => {
  const [error, setError] = React.useState<string>("");

  const handleSubmit = (
    values: PasswordRevcoveryFormValues,
    actions: FormikHelpers<any>
  ) => {
    recoverPasswordByEmail(values.email)
      .then((response) => {
        onSend(
          values.email,
          response.data.token,
          response.data.expires,
          response.data.resend_in
        );
      })
      .catch(({ errors, message }) => {
        actions.setErrors(errors);
        actions.setSubmitting(false);
        if (errors.length === 0) {
          setError(message);
        }
      });
  };

  return (
    <>
      <Formik
        initialValues={{ email: "" }}
        onSubmit={handleSubmit}
        validateOnBlur={false}
        validateOnChange={false}
        validationSchema={VALIDATION_SCHEMA}>
        {({ isSubmitting, errors, setErrors }) => {
          return (
            <Form>
              <div
                className={
                  errors.email
                    ? `form-group field-input mb-1 error-input-field`
                    : "form-group field-input mb-1"
                }>
                <label className='col-form-label fw-600 labForBook'>
                  Email
                </label>
                <div className='input-group'>
                  <Field name='email' className='form-control mb-0' />
                </div>
              </div>
              {errors.email && (
                <div className='container px-0'>
                  <div className='d-flex justify-content-between alert alert-danger py-2'>
                    <div className='d-flex align-item-center mx-0 py-0'>
                      <ul className='list-unstyled error-message-body my-0 d-flex flex-column justify-content-center'>
                        <ErrorMessage
                          name='email'
                          component='span'
                          className='text-danger'
                        />
                      </ul>
                    </div>
                    <div className=''>
                      <div
                        className='d-flex justify-content-end align-items-center flex-grow-1 flex-fill'
                        style={{ height: "100%" }}>
                        <button
                          onClick={() => setErrors({})}
                          className='border-0 bg-transparent text-danger'>
                          <FontAwesomeIcon icon={faTimes} />
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              )}
              <div className='mt-4'>
                <button
                  disabled={isSubmitting}
                  type='submit'
                  value='Send'
                  className='my-sm-0 w-100  primary-large-button padding5 borderR4 font-weight-bold'>
                  {isSubmitting ? (
                    <FontAwesomeIcon icon={faSpinner} spin />
                  ) : (
                    "Send"
                  )}
                </button>
              </div>
              {error !== "" && <span className='text-danger'> {error}</span>}
            </Form>
          );
        }}
      </Formik>

      <div className='text-center mt-2'>
        <Link
          to={signinUrl}
          title='Sign In'
          className='text-decoration-none text-link-color'>
          Return to login
        </Link>
      </div>

      {supportUrl !== "" && (
        <div className='text-center mt-2'>
          Other problems logging in?{" "}
          <a
            href={supportUrl}
            className='text-decoration-none text-link-color fw-600'
            target='_blank'>
            Contact us
          </a>
        </div>
      )}
    </>
  );
};

const PasswordRevcovery = ({
  appTitle,
  serviceName,
  signinUrl,
  supportUrl,
  logo,
  onRevcover,
  onReset,
  ...props
}: PasswordRevcoveryI) => {
  const [screen, setScreen] = React.useState<Screen>({
    isSend: false,
    token: "",
    ttl: 0,
    email: "",
    resendTtl: 0,
  });
  const onSend = (
    email: string,
    token: string,
    ttl: number,
    resendTtl: number
  ) => {
    setScreen((prev) => {
      return { ...prev, email, token, ttl, isSend: true };
    });
    onRevcover();
  };

  return (
    <div className='signin-body h-100'>
      <section className='p-md-5 pt-2 pb-2 h-100'>
        <div className='container h-100'>
          {/* <img
                src={forgetImage}
                className="img-fluid"
                style={{
                    width: "400px",
                    position: "absolute",
                    top: "10%",
                    right: "-5%",
                }}
            /> */}

          <div className='row h-100 justify-content-center'>
            <div className='col-md-5 px-4 m-md-auto flex-mob-limit pb-5'>
              <div className='p-5 bg-white shadow-lg'>
                <div className='d-flex flex-column align-items-center mb-3'>
                  {screen.isSend ? (
                    <>
                      <h6 className='signup-head'>Reset your password</h6>
                      <p className='mb-2 text-muted'>
                        OTP sent to your email <b>{screen.email}</b>. If not
                        seen in inbox check your spam folder
                      </p>
                    </>
                  ) : (
                    <h6>Can't Login?</h6>
                  )}
                  <div className='d-flex justify-content-center'>
                    <img style={{ width: "100px" }} src={logo} alt='Logo' />
                  </div>
                </div>
                {screen.isSend ? (
                  <>
                    <PasswordResetOtp
                      appTitle={appTitle}
                      serviceName={serviceName}
                      token={screen.token}
                      email={screen.email}
                      signinUrl={signinUrl}
                      logo={logo}
                      onReset={onReset}
                    />
                  </>
                ) : (
                  <SendRecovery
                    appTitle={appTitle}
                    onSend={onSend}
                    signinUrl={signinUrl}
                    supportUrl={supportUrl}
                  />
                )}
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default PasswordRevcovery;
