import { Field, Form, Formik, FormikHelpers, ErrorMessage } from "formik";
import React from "react";
import { accountVerify } from "./api/auth";
import { Values } from "./api";
import "./style.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import Otp from './otp';

interface VerifyAccountFormValues extends Values {
  code: string;
}

interface VerifyAccountI {
  serviceName: string;
  appTitle: string;
  token: string;
  email: string;
  code?: string;
  logo?: string;
  onVerify: () => void;
}

interface Screen {
  name: string;
  error: string;
}

const VerifyAccountOtp = ({
  appTitle,
  logo,
  onVerify,
  ...props
}: VerifyAccountI) => {
  const [screen, setScreen] = React.useState<Screen>({
    name: "account-verification-otp",
    error: "",
  });
  
  const handleSubmit = (
    values: VerifyAccountFormValues,
    actions: FormikHelpers<any>
  ) => {
    
    accountVerify({
      ...values,
      type: "email",
      token: props.token,
      email: props.email,
    })
      .then((response) => {
        setScreen((prev) => {
          return { ...prev, name: "success" };
        });
        onVerify();
      })
      .catch(({ errors, message }) => {
        actions.setErrors(errors);
        actions.setSubmitting(false);
        if (errors.length === 0) {
          setScreen((prev) => {
            return { ...prev, error: message };
          });
        }
      });
  };

  if (screen.name === "success") {
    return (
      <>
        <div className='text-center'>
          <h3 className='signup-head'>Account verification success</h3>
          <div className='row justify-content-center py-3'>
            <div className='text-center'>
              <p className='mb-2 text-muted'>
                Your account successfully verified
              </p>
            </div>
          </div>
        </div>
      </>
    );
  }

  return (
    <>
      <Formik
        initialValues={{ code: props.code ?? ""}}
        onSubmit={handleSubmit}>
        {({ isSubmitting, setFieldValue }) => {
          return (
            <Form>
              {!props.code === true && (
                <div className='form-group field-input-register'>
                  <label className='col-form-label fw-600 labForBook'>
                    Enter 6 digit OTP recived on your email <span className="text-muted">{props.email}</span>
                  </label>
                  <Otp onChange={(val) => setFieldValue('code', val)} />
                  <ErrorMessage
                    name='code'
                    component='span'
                    className='text-danger'
                  />
                </div>
              )}

              {screen.error !== "" && (
                <span className='text-danger'> {screen.error}</span>
              )}

              <div className='mt-4'>
                <button
                  disabled={isSubmitting}
                  type='submit'
                  value='Verify'
                  className='my-sm-0 w-100  primary-large-button padding5 borderR4 font-weight-bold'>
                  {isSubmitting ? (
                    <FontAwesomeIcon icon={faSpinner} spin />
                  ) : (
                    "Verify"
                  )}
                </button>
              </div>
            </Form>
          );
        }}
      </Formik>
    </>
  );
};

export default VerifyAccountOtp;
