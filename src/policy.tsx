import React from "react";

interface PolicyI {
  appTitle: string;
  termsUrl?: string;
  protectionUrl?: string;
  PrivacyUrl?: string;
}

const Policy = ({ appTitle, termsUrl, protectionUrl, PrivacyUrl }: PolicyI) => {
  return (
    <div className='border-top mt-3 pt-4 f-11'>
      <p className='text-muted text-justify'>
        By creating an account, you agree to {appTitle}'s{" "}
        <a
          href={termsUrl}
          className='text-decoration-none text-link-color'
          target='_blank'>
          Terms of Service,
        </a>{" "}
        <a
          href={protectionUrl}
          className='text-decoration-none text-link-color'
          target='_blank'>
          Data Protection
        </a>{" "}
        and{" "}
        <a
          href={PrivacyUrl}
          className='text-decoration-none text-link-color'
          target='_blank'>
          Privacy Policy.
        </a>{" "}
        You consent to receiving marketing messages from {appTitle} and may opt
        out from receiving such messages by following the unsubscribe link in
        our messages, or as detailed in our terms
      </p>
    </div>
  );
};

export default Policy;
