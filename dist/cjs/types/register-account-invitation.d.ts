/// <reference types="react" />
import './style.css';
import { Token } from './types';
interface RegisterI {
    useSocial?: boolean;
    serviceName: string;
    appTitle: string;
    resetPasswordUrl?: string;
    signinUrl?: string;
    termsUrl?: string;
    protectionUrl?: string;
    PrivacyUrl?: string;
    token: string;
    supportUrl: string;
    onRegister: (token: Token) => void;
    logo?: string;
}
declare const Register: ({ appTitle, resetPasswordUrl, signinUrl, termsUrl, protectionUrl, PrivacyUrl, supportUrl, logo, token, ...props }: RegisterI) => JSX.Element;
export default Register;
