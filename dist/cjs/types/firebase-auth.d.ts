/// <reference types="react" />
import 'firebase/compat/auth';
interface FirbaseAuthI {
    onSuccess: (token: any) => void;
    label: string;
}
declare const FirbaseAuth: ({ onSuccess, label }: FirbaseAuthI) => JSX.Element;
export default FirbaseAuth;
