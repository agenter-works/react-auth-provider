/// <reference types="react" />
interface OtpI {
    inputSize: number;
    isInputNum: boolean;
    onChange: (value: string) => void;
}
declare const Otp: {
    ({ inputSize, isInputNum, onChange }: OtpI): JSX.Element;
    defaultProps: {
        isInputNum: boolean;
        inputSize: number;
    };
};
export default Otp;
