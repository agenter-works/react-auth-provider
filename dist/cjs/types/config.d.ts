interface ConfigI {
    setUrl: (url: string) => void;
    getUrl: () => string;
    setFirebaseConfig: (config: any) => void;
    getFirebaseConfig: () => string;
}
declare const Config: ConfigI;
export default Config;
