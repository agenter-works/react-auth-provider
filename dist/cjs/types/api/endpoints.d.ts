declare const endPoints: {
    register: string;
    registerWithProvider: string;
    registerOnInvitation: string;
    login: string;
    profile: string;
    recoverPassword: string;
    resetPassword: string;
    accountVerify: string;
};
export { endPoints };
