/// <reference types="react" />
interface PolicyI {
    appTitle: string;
    termsUrl?: string;
    protectionUrl?: string;
    PrivacyUrl?: string;
}
declare const Policy: ({ appTitle, termsUrl, protectionUrl, PrivacyUrl }: PolicyI) => JSX.Element;
export default Policy;
