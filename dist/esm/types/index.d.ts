export { default as Login } from './login';
export { default as Register } from './register';
export { default as PassordReset } from './password-reset';
export { default as PassordRecovery } from './password-recovery';
export { default as VerifyAccount } from './verify-account';
export { default as Otp } from './otp';
export { default as Config } from './config';
export { default as RegisteronInvitation } from './register-account-invitation';
