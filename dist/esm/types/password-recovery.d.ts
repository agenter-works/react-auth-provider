/// <reference types="react" />
import "./style.css";
interface PasswordRevcoveryI {
    serviceName: string;
    appTitle: string;
    signinUrl: string;
    supportUrl?: string;
    onRevcover: () => void;
    onReset: () => void;
    logo?: string;
}
declare const PasswordRevcovery: ({ appTitle, serviceName, signinUrl, supportUrl, logo, onRevcover, onReset, ...props }: PasswordRevcoveryI) => JSX.Element;
export default PasswordRevcovery;
