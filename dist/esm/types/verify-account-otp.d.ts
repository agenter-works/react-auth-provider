/// <reference types="react" />
import "./style.css";
interface VerifyAccountI {
    serviceName: string;
    appTitle: string;
    token: string;
    email: string;
    code?: string;
    logo?: string;
    onVerify: () => void;
}
declare const VerifyAccountOtp: ({ appTitle, logo, onVerify, ...props }: VerifyAccountI) => JSX.Element;
export default VerifyAccountOtp;
