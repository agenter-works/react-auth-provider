/// <reference types="react" />
import './style.css';
interface PasswordResetI {
    serviceName: string;
    appTitle: string;
    supportUrl: string;
    onReset: () => void;
    logo?: string;
}
declare const PasswordReset: ({ appTitle, serviceName, supportUrl, logo, onReset, ...props }: PasswordResetI) => JSX.Element;
export default PasswordReset;
