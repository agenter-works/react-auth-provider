/// <reference types="react" />
import "./style.css";
interface PasswordResetI {
    serviceName: string;
    appTitle: string;
    token: string;
    email: string;
    signinUrl: string;
    code?: string;
    logo?: string;
    onReset: () => void;
}
declare const PasswordResetOtp: ({ appTitle, signinUrl, logo, onReset, ...props }: PasswordResetI) => JSX.Element;
export default PasswordResetOtp;
