export interface Token {
    token: string;
    ttl: number;
    expire_in: number;
}
