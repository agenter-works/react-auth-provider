/// <reference types="react" />
import "./style.css";
import { Token } from "./types";
interface LoginI {
    useSocial?: boolean;
    serviceName: string;
    appTitle: string;
    resetPasswordUrl?: string;
    registerUrl?: string;
    onLogin: (token: Token) => void;
    logo?: string;
    termsUrl?: string;
    protectionUrl?: string;
    PrivacyUrl?: string;
    networkAccess?: string;
}
declare const Login: ({ appTitle, resetPasswordUrl, registerUrl, termsUrl, protectionUrl, PrivacyUrl, logo, useSocial, networkAccess, ...props }: LoginI) => JSX.Element;
export default Login;
