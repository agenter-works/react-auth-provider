/// <reference types="react" />
import "./style.css";
interface VerifyAccountI {
    serviceName: string;
    appTitle: string;
    supportUrl: string;
    signinUrl: string;
    onVerify: () => void;
    logo?: string;
}
declare const VerifyAccount: ({ appTitle, serviceName, supportUrl, signinUrl, logo, onVerify, ...props }: VerifyAccountI) => JSX.Element;
export default VerifyAccount;
