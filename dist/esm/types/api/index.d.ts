export interface Values {
    [key: string]: string | number | boolean;
}
declare const instance: import("axios").AxiosInstance;
export default instance;
export interface ReadI {
    url: string;
    id?: number | string;
    params?: any;
}
export interface CreateI extends ReadI {
    data?: any;
    headers?: any;
}
declare const getData: ({ url, params, id }: ReadI) => Promise<import("axios").AxiosResponse<any, any>>;
declare const postData: ({ url, data, params, id }: CreateI) => Promise<import("axios").AxiosResponse<any, any>>;
export { postData, getData };
