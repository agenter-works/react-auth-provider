/// <reference types="react" />
interface Token {
    token: string;
    ttl: number;
    expire_in: number;
}

interface LoginI {
    useSocial?: boolean;
    serviceName: string;
    appTitle: string;
    resetPasswordUrl?: string;
    registerUrl?: string;
    onLogin: (token: Token) => void;
    logo?: string;
    termsUrl?: string;
    protectionUrl?: string;
    PrivacyUrl?: string;
    networkAccess?: string;
}
declare const Login: ({ appTitle, resetPasswordUrl, registerUrl, termsUrl, protectionUrl, PrivacyUrl, logo, useSocial, networkAccess, ...props }: LoginI) => JSX.Element;

interface RegisterI$1 {
    useSocial?: boolean;
    serviceName: string;
    appTitle: string;
    resetPasswordUrl?: string;
    signinUrl?: string;
    termsUrl?: string;
    protectionUrl?: string;
    PrivacyUrl?: string;
    supportUrl: string;
    onRegister: (token: Token) => void;
    logo?: string;
}
declare const Register$1: ({ appTitle, resetPasswordUrl, signinUrl, termsUrl, protectionUrl, PrivacyUrl, supportUrl, useSocial, logo, ...props }: RegisterI$1) => JSX.Element;

interface PasswordResetI {
    serviceName: string;
    appTitle: string;
    supportUrl: string;
    onReset: () => void;
    logo?: string;
}
declare const PasswordReset: ({ appTitle, serviceName, supportUrl, logo, onReset, ...props }: PasswordResetI) => JSX.Element;

interface PasswordRevcoveryI {
    serviceName: string;
    appTitle: string;
    signinUrl: string;
    supportUrl?: string;
    onRevcover: () => void;
    onReset: () => void;
    logo?: string;
}
declare const PasswordRevcovery: ({ appTitle, serviceName, signinUrl, supportUrl, logo, onRevcover, onReset, ...props }: PasswordRevcoveryI) => JSX.Element;

interface VerifyAccountI {
    serviceName: string;
    appTitle: string;
    supportUrl: string;
    signinUrl: string;
    onVerify: () => void;
    logo?: string;
}
declare const VerifyAccount: ({ appTitle, serviceName, supportUrl, signinUrl, logo, onVerify, ...props }: VerifyAccountI) => JSX.Element;

interface OtpI {
    inputSize: number;
    isInputNum: boolean;
    onChange: (value: string) => void;
}
declare const Otp: {
    ({ inputSize, isInputNum, onChange }: OtpI): JSX.Element;
    defaultProps: {
        isInputNum: boolean;
        inputSize: number;
    };
};

interface ConfigI {
    setUrl: (url: string) => void;
    getUrl: () => string;
    setFirebaseConfig: (config: any) => void;
    getFirebaseConfig: () => string;
}
declare const Config: ConfigI;

interface RegisterI {
    useSocial?: boolean;
    serviceName: string;
    appTitle: string;
    resetPasswordUrl?: string;
    signinUrl?: string;
    termsUrl?: string;
    protectionUrl?: string;
    PrivacyUrl?: string;
    token: string;
    supportUrl: string;
    onRegister: (token: Token) => void;
    logo?: string;
}
declare const Register: ({ appTitle, resetPasswordUrl, signinUrl, termsUrl, protectionUrl, PrivacyUrl, supportUrl, logo, token, ...props }: RegisterI) => JSX.Element;

export { Config, Login, Otp, PasswordRevcovery as PassordRecovery, PasswordReset as PassordReset, Register$1 as Register, Register as RegisteronInvitation, VerifyAccount };
